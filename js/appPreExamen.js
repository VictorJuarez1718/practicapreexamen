function ajax(){
    const url = "https://jsonplaceholder.typicode.com/users";


    axios.get(url)
    .then((res)=>{
        mostrar(res.data);
    })
    .catch((err)=>{
        console.log("Ha ocurrido un error" + err);
    })


    function mostrar(data){

       // const res = document.getElementById('respuesta');
        //res.innerHTML = "";

        var numID = document.getElementById("idPrincipal").value
        var hola = document.getElementById("idPrincipal")
        var nombreTexto = document.getElementById("nombre")
        var nombreUsuario = document.getElementById("nombreUsuario")
        var email = document.getElementById("email")
        var calle = document.getElementById("calle")
        var numero = document.getElementById("numero")
        var ciudad = document.getElementById("ciudad")
        var bandera = false
        

        for(item of data){
            if(item.id == numID){
                nombreTexto.value = item.name
                nombreUsuario.value = item.username
                email.value = item.email
                calle.value = item.address.street
                numero.value = item.address.suite
                ciudad.value = item.address.city
            }
            if(numID == 0 || numID == "" || numID > 10){
                   if(!bandera){
                        alert("No se puede colocar ID 0, dejar vacío el espacio o colocar un ID mayor a 10")
                       bandera = true
                    }
                    
                    
            }
                
            document.getElementById("btnLimpiar").addEventListener("click", function(){
                hola.value = ""
                nombreTexto.value = ""
                nombreUsuario.value = ""
                email.value = ""
                calle.value = ""
                numero.value = ""
                ciudad.value = ""
            })
        } //for

    }// mostrar




}  //Ajax

document.getElementById("btnCargar").addEventListener("click", function(){ajax();})